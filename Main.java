public abstract class Main {
    public static void main(String[] args) {
        ApplePhone applePhone = new ApplePhone();
        System.out.println("Applephone can operate such functions : \n");

        applePhone.call();
        applePhone.sendSms();
        applePhone.recordVideo();
        applePhone.takePhotos();

        XiomiPhone xiomiPhone = new XiomiPhone();
        System.out.println("Xiomiphone can operate such functions : \n");
        xiomiPhone.call();
        xiomiPhone.sendSms();
    }


    }




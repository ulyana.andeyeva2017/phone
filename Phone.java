public abstract class Phone {
String name,model,OperatingSystem;
int RAM,MainMemory;

    public  Phone(String name,String model,String OperatingSystem, int RAM,int MainMemory) {
        this.name = name;
        this.model = model;
        this.OperatingSystem = OperatingSystem;
        this.RAM = RAM;
        this.MainMemory = MainMemory;
    }
}

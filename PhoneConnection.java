public  interface PhoneConnection {
    public void call(); // interface method (does not have a body)

    public void sendSms();
}
